//
//  ViewController.m
//  ScrollView
//
//  Created by Tyler Barnes on 1/11/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView * scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:scrollView];
    scrollView.pagingEnabled = YES;
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    
    
    UILabel * redView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    redView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:redView];
    redView.text = @"1";
    redView.textAlignment = NSTextAlignmentCenter;
    redView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * blueView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    blueView.backgroundColor = [UIColor blueColor];
    [scrollView addSubview:blueView];
    blueView.text = @"2";
    blueView.textAlignment = NSTextAlignmentCenter;
    blueView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * blackView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    blackView.backgroundColor = [UIColor blackColor];
    [scrollView addSubview:blackView];
    blackView.text = @"3";
    blackView.textAlignment = NSTextAlignmentCenter;
    blackView.font = [UIFont systemFontOfSize: 300];
    blackView.textColor = [UIColor whiteColor];
    
    
    UILabel * grayView = [[UILabel alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height)];
    grayView.backgroundColor = [UIColor grayColor];
    [scrollView addSubview:grayView];
    grayView.text = @"4";
    grayView.textAlignment = NSTextAlignmentCenter;
    grayView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * whiteView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:whiteView];
    whiteView.text = @"5";
    whiteView.textAlignment = NSTextAlignmentCenter;
    whiteView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * greenView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height)];
    greenView.backgroundColor = [UIColor greenColor];
    [scrollView addSubview:greenView];
    greenView.text = @"6";
    greenView.textAlignment = NSTextAlignmentCenter;
    greenView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * purpleView = [[UILabel alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    purpleView.backgroundColor = [UIColor purpleColor];
    [scrollView addSubview:purpleView];
    purpleView.text = @"7";
    purpleView.textAlignment = NSTextAlignmentCenter;
    purpleView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * yellowView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    yellowView.backgroundColor = [UIColor yellowColor];
    [scrollView addSubview:yellowView];
    yellowView.text = @"8";
    yellowView.textAlignment = NSTextAlignmentCenter;
    yellowView.font = [UIFont systemFontOfSize: 300];
    
    
    UILabel * orangeView = [[UILabel alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollView addSubview:orangeView];
    orangeView.text = @"9";
    orangeView.textAlignment = NSTextAlignmentCenter;
    orangeView.font = [UIFont systemFontOfSize: 300];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
